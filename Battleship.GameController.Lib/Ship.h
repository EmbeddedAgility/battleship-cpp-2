#pragma once

#include <string>
#include <list>

#include "Position.h"

namespace Battleship
{
  namespace GameController
  {
	namespace Contracts
	{
	  class Ship
	  {

	  public:
		Ship();
		Ship(std::string Name, int Size);
		~Ship();

	  public:

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		std::string Name;

		/// <summary>
		/// Gets or sets the positions.
		/// </summary>
		std::list<Position> Positions;
		std::list<Position> hitPositions;

		/// <summary>
		/// Gets or sets the size.
		/// </summary>
		int Size;

		bool isSunken() const;

		/// <summary>
		/// The add position.
		/// </summary>
		/// <param name="input">
		/// The input.
		/// </param>
		void AddPosition(std::string input);
		void AddPosition(const Position &input);
		void AddHitPosition(const Position &input);
	  };
	}
  }
}

