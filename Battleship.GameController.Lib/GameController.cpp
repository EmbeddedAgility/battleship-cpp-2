#include "GameController.h"
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include <stdexcept>
#include <iostream>

namespace Battleship {
namespace GameController {
using namespace std;

GameController::GameController() {}

GameController::~GameController() {}

bool GameController::CheckIsHit(std::list<Ship>& ships, Position shot) {
  if (&ships == nullptr)
    throw std::invalid_argument(std::string("argument 'ships' is invalid"));

  for (const auto& ship : ships) {
    for (const auto& p : ship.Positions) {
      if (p == shot) {
        return true;
      }
    }
  }
  return false;
}

void GameController::SetShipToHit(std::list<Ship>& ships, Position shot) {
  if (&ships == nullptr)
    throw std::invalid_argument(std::string("argument 'ships' is invalid"));

  for (auto& ship : ships) {
    for (const auto& p : ship.Positions) {
      if (p == shot) {
       // cout << shot.Column << shot.Row;
        ship.AddHitPosition(shot);
        break;
      }
    }
  }
}

list<Ship> GameController::InitializeShips() {
  list<Ship> ships;
  ships.insert(ships.end(), Ship("Aircraft Carrier", 5));
 /* ships.insert(ships.end(), Ship("Battleship", 4));
  ships.insert(ships.end(), Ship("Submarine", 3));
  ships.insert(ships.end(), Ship("Destroyer", 3));
  ships.insert(ships.end(), Ship("Patrol Boat", 2));*/

  return ships;
}

list<Ship> GameController::InitializeEnemyShips() {
  list<Ship> ships;
 // ships.insert(ships.end(), Ship("Aircraft Carrier", 5));
  ships.insert(ships.end(), Ship("Battleship", 4));
 // ships.insert(ships.end(), Ship("Submarine", 3));
 // ships.insert(ships.end(), Ship("Destroyer", 3));
  ships.insert(ships.end(), Ship("Patrol Boat", 2));

  return ships;
}

bool GameController::IsShipsValid(Ship ship) {
  return ship.Positions.size() == ship.Size;
}

Position GameController::GetRandomPosition(int size) {
  srand((unsigned int)time(NULL));
  Letters lColumn = (Letters)(rand() % size);
  int nRow = (rand() % size);

  Position position(lColumn, nRow);
  return position;
}
}  // namespace GameController
}  // namespace Battleship

