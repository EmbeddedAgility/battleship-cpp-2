#pragma once

#include <list>

#include "../Battleship.GameController.Lib/Ship.h"

using namespace std;
using namespace Battleship::GameController::Contracts;

namespace Battleship
{
  namespace Ascii
  {
	class Program
	{
	private:
	  static list<Ship> myFleet;
	  static list<Ship> enemyFleet;

	  static list<Position> enemyHits;
	  static list<Position> enemyMissed;

	  static list<Position> myHits;
	  static list<Position> myMissed;

	  static int rows;
	  static int columns;

	public:
	  Program();
	  ~Program();

	    enum Code {
            FG_RED      = 31,
            FG_GREEN    = 32,
            FG_BLUE     = 34,
            FG_DEFAULT  = 39
        };

	public:
	  static void Main();
	  static void StartGame();

	private:
	  static void InitializeGame();

	  static void InitializeMyFleet();
	  static void InitializeEnemyFleet(list<Ship> &Fleet);
	  static bool checkPositionInList(Position currentPosition, const list<Position> &list);
	  static void printColorText(std::string str, Battleship::Ascii::Program::Code colorCode);
	  static bool IsShipSunken(Position pos, const list<Ship> & ships);

	public:
	  static Position ParsePosition(string input);
	  static Position GetRandomPosition();
	  static void showEnemyBoard();
	  static void showMyBoard();
      static void showWinMsg();
      static void showLoseMsg();
      static void reset();
      static string GetShipName(Position pos, const list<Ship> & ships);
	};
  }
}

