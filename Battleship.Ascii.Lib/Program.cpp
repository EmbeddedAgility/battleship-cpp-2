#include "Program.h"

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <algorithm>

#include "../Battleship.GameController.Lib/GameController.h"

#pragma comment(lib,"winmm.lib")  //for MSV C++   

using namespace Battleship::GameController;
using namespace Battleship::GameController::Contracts;

namespace Battleship
{
  namespace Ascii
  {
    ostream& operator<<(ostream &out, Position pos)
    {
      out << (char)('A' + pos.Column) << pos.Row;
      return out;
    }

    Program::Program()
    {
    }

    Program::~Program()
    {
    }

    list<Ship> Program::myFleet;
    list<Ship> Program::enemyFleet;

	list<Position> Program::enemyHits;
	list<Position> Program::enemyMissed;

	list<Position> Program::myHits;
	list<Position> Program::myMissed;

    int playerHit; 
    int computerHit;
    int totalHits;

    int Program::rows = 8;
    int Program::columns = 8;
    
    void Program::Main()
    {
      cout << R"(                                     |__                                       )" << endl;
      cout << R"(                                     | \ /                                     )" << endl;
      cout << R"(                                     ---                                       )" << endl;
      cout << R"(                                     / | [                                     )" << endl;
      cout << R"(                              !      | |||                                     )" << endl;
      cout << R"(                            _/|     _/|-++'                                    )" << endl;
      cout << R"(                        +  +--|    |--|--|_ |-                                 )" << endl;
      cout << R"(                     { /|__|  |/\__|  |--- |||__/                              )" << endl;
      cout << R"(                    +---------------___[}-_===_.'____                 /\       )" << endl;
      cout << R"(                ____`-' ||___-{]_| _[}-  |     |_[___\==--            \/    _  )" << endl;
      cout << R"( __..._____--==/___]_|__|_____________________________[___\==--____,------' .7 )" << endl;
      cout << R"(|                        Welcome to Battleship                         BB-61/  )" << endl;
      cout << R"( \_________________________________________________________________________|   )" << endl;
      cout << endl;
      cout << "\033[0m";

	  InitializeGame();

      StartGame();
    }

    void Program::StartGame()
    {
      //Console::Clear();
      /*
      cout << R"(                  __     )" << endl;
      cout << R"(                 /  \    )" << endl;
      cout << R"(           .-.  |    |   )" << endl;
      cout << R"(   *    _.-'  \  \__/    )" << endl;
      cout << R"(    \.-'       \         )" << endl;
      cout << R"(   /          _/         )" << endl;
      cout << R"(  |      _  /""          )" << endl;
      cout << R"(  |     /_\'             )" << endl;
      cout << R"(   \    \_/              )" << endl;
      cout << R"(    """"""""             )" << endl;
*/

printColorText("#######################################################", FG_GREEN);
const string text = "\n"
"\n"
"   _____                           ____                    \n"
"  / ____|                         |  _ \\                  \n"
" | (___   ___ _ __ _   _ _ __ ___ | |_) | __ _  __ _ ___   \n"
"  \\___ \\ / __| '__| | | | '_ ` _ \\|  _ < / _` |/ _` / __| \n"
"  ____) | (__| |  | |_| | | | | | | |_) | (_| | (_| \\__ \\ \n"
" |_____/ \\___|_|   \\__,_|_| |_| |_|____/ \\__,_|\\__, |___/ \n"
"                                                __/ |     \n"
"                                               |___/      \n"
"\n"
"\n";

printColorText(text, FG_RED);
printColorText("#######################################################\n", FG_GREEN);

      do
      {
        cout << endl;
        cout << R"(Player, it's your turn   )" << endl;

		bool PositionValid = false;
		string input;
		Position position;

		cout << R"(Enter coordinates for your shot :   )" << endl;
		getline(cin, input);

		position = ParsePosition(input);

        if (checkPositionInList(position, myHits) || checkPositionInList(position, myMissed)) {
            cout << "You have already attacked this position" << endl;
            continue;
        }

        bool isHit = GameController::GameController::CheckIsHit(enemyFleet, position);

        if (isHit)
        {
            // Console::Beep();
/*
			cout << R"(                \         .  ./         )" << endl;
            cout << R"(              \      .:"";'.:..""   /   )" << endl;
            cout << R"(                  (M^^.^~~:.'"").       )" << endl;
            cout << R"(            -   (/  .    . . \ \)  -    )" << endl;
            cout << R"(               ((| :. ~ ^  :. .|))      )" << endl;
            cout << R"(            -   (\- |  \ /  |  /)  -    )" << endl;
            cout << R"(                 -\  \     /  /-        )" << endl;
            cout << R"(                   \  \   /  /          )" << endl;

            */
          printColorText("Yeah ! Nice hit !", FG_GREEN);
          cout << endl;

          playerHit++;
          GameController::GameController::SetShipToHit(enemyFleet, position);
          myHits.push_back(position);

        if (IsShipSunken(position, enemyFleet)) {
            string shipName =  GetShipName(position, enemyFleet);
          //  cout << endl;
           // printColorText("Sunked " + shipName, FG_GREEN);
           // cout << endl;

cout << endl;
printColorText("#################################", FG_RED); 
cout << endl;
printColorText("Ship sunked : " + shipName, FG_GREEN);
cout << endl;
printColorText("#################################", FG_RED); 
cout << endl;

        }

          if (playerHit == totalHits) {
           // cout << "You win!" << endl;
           showWinMsg();
          //  cout << "Game End!" << endl;
           // return;

           cout << "That was luck! You want to try again (Y | N)" << endl;

           while (1) {
               string result;
               getline(cin, result);
               
               if (result == "Y" || result == "y") {
                   reset();
                   InitializeGame();
                   StartGame();
               } else if (result == "N" || result == "n") {
                   cout << "CHICKEN!!!" << endl; 
                   return;
                } else {
                    cout << "Enter Y or y for yes and N or n for No" << endl;
                }
          }
          }



		}
		else
		{
            printColorText("Miss", FG_RED);
            cout << endl;
            myMissed.push_back(position);
		}

        bool isMissedOutSide = false;

        if (position.Column < Letters::A || position.Column > Letters::H) {
            cout << "You shoot outside of the field" << endl;
            isMissedOutSide = true;
        } else if (position.Row <= 0 || position.Row > 8) {
            cout << "You shoot outside of the field" << endl;
            isMissedOutSide = true;
        }

        if (isMissedOutSide) {
            continue;
        }

        position = GetRandomPosition();
        isHit = GameController::GameController::CheckIsHit(myFleet, position);
        cout << endl;

        if (isHit)
        {
            //Console::Beep();
/*
			cout << R"(                \         .  ./         )" << endl;
            cout << R"(              \      .:"";'.:..""   /   )" << endl;
            cout << R"(                  (M^^.^~~:.'"").       )" << endl;
            cout << R"(            -   (/  .    . . \ \)  -    )" << endl;
            cout << R"(               ((| :. ~ ^  :. .|))      )" << endl;
            cout << R"(            -   (\- |  \ /  |  /)  -    )" << endl;
            cout << R"(                 -\  \     /  /-        )" << endl;
            cout << R"(                   \  \   /  /          )" << endl;
*/
          cout << "(Computer shoot in " << position << " and " << "hit your ship !)" << endl;
          computerHit++;

        if (IsShipSunken(position, myFleet)) {
            string shipName =  GetShipName(position, myFleet);
            cout << "Your ship is sunked " << shipName;
        }

          if (computerHit == totalHits) {
           // cout << "You lost!" << endl;
           // cout << "Game End!" << endl;
           showLoseMsg();
        
           cout << "Want to try again (Y | N)";

           while (1) {
               string result;
               getline(cin, result);
               
               if (result == "Y" || result == "y") {
                   reset();
                   InitializeGame();
                   StartGame();
               } else if (result == "N" || result == "n") {
                   cout << "CHICKEN!!!" << endl; 
                   return;
                } else {
                    cout << "Enter Y or y for yes and N or n for No" << endl;
                }
          }
          }

          enemyHits.push_back(position);
        }
		else
		{
            cout << "(Computer shoot in " << position << " and missed )   " << endl;
            enemyMissed.push_back(position);
		}

        cout << "Enemy board!" << endl;
        showEnemyBoard();
        printColorText("\n", FG_DEFAULT);

        cout << "My board!" << endl;
        printColorText("\n", FG_DEFAULT);

        showMyBoard();
        printColorText("\n", FG_DEFAULT);
      }
      while (true);
    }

    void Program::reset()
    {
        myFleet.clear();
        enemyFleet.clear();
        enemyHits.clear();
        enemyMissed.clear();
        myHits.clear();
        myMissed.clear();

        playerHit = 0;
        computerHit = 0;
        totalHits = 0;
    }

    void Program::showEnemyBoard()
    {
        cout  << "  ";
        for (int j = 0; j < columns; ++j) {
            char c = 'A';
            cout  << " " << string(1, c + j) << " ";
        }

        cout << endl;

		for (int i = 1; i <= rows; ++i) {
            cout << i << " ";
            for (int j = 0; j < columns; ++j) {
				Position currentPosition;
				currentPosition.Column = (Letters)j;
				currentPosition.Row = i;

                if (IsShipSunken(currentPosition, enemyFleet)) {
                    printColorText(" x ", FG_RED);
                } else if (checkPositionInList(currentPosition, myHits)) {
                    printColorText(" o ", FG_RED);
				} else if (checkPositionInList(currentPosition, myMissed)) {
					printColorText(" o ", FG_GREEN);
				} else {
					printColorText(" o ", FG_BLUE);
				}
			}
			cout << endl;
		}
	}

	void Program::showMyBoard()
	{
        cout  << "  ";

        for (int j = 0; j < columns; ++j) {
            char c = 'A';
            cout  << " " << string(1, c + j) << " ";
        }

        cout << endl;

        for (int i = 1; i <= rows; ++i) {
            cout << i << " ";
            for (int j = 0; j < columns; ++j) {

                Position currentPosition;
				currentPosition.Column = (Letters)j;
				currentPosition.Row = i;

				if (checkPositionInList(currentPosition, enemyHits)) {
                    printColorText(" o ", FG_RED);
				} else if (checkPositionInList(currentPosition, enemyMissed)) {
					printColorText(" o ", FG_GREEN);
				} else {
					printColorText(" o ", FG_BLUE);
				}
			}

			cout << endl;
		}
	}

  bool Program::checkPositionInList(Position currentPosition, const list<Position> &list) {
		for (const auto &hitPosition : list) {
			if (currentPosition.Column == hitPosition.Column && currentPosition.Row == hitPosition.Row) {
				return true;
			}
		}

		return false;
	}

bool Program::IsShipSunken(Position pos, const list<Ship> & ships) {
    for (const auto& ship : ships) {
        for (const auto& p : ship.Positions) {
            if (p == pos) {
                if (ship.isSunken()) {
                    return true;
                }
            }
        }
    }

  return false;
}

string Program::GetShipName(Position pos, const list<Ship> & ships) {
    for (const auto& ship : ships) {
        for (const auto& p : ship.Positions) {
            if (p == pos) {
                return ship.Name;
            }
        }
    }

  return "";
}

void Program::showWinMsg()
{
    cout<<"***********************************************"<<endl;	
	cout<<"__   _____  _   _  __        _____ _   _    _ "<<endl;
	cout<<"\\ \\ / / _ \\| | | | \\ \\      / /_ _| \\ | |  | |"<<endl;
	cout<<" \\ V / | | | | | |  \\ \\ /\\ / / | ||  \\| |  | |"<<endl;
	cout<<"  | || |_| | |_| |   \\ V  V /  | || |\\  |  |_|"<<endl;
	cout<<"  |_| \\___/ \\___/     \\_/\\_/  |___|_| \\_|  (_)"<<endl;
	cout<<"                                            "<<endl; 
	cout<<"***********************************************"<<endl;
}

void Program::showLoseMsg()
{
cout<<"+ + + + + + + + + + + + + + + + + + + + + + + +"<<endl;	
	cout<<"__   _____  _   _     _     ___  ____  _____ _  "<<endl;
	cout<<"\\ \\ / / _ \\| | | |   | |   / _ \\/ ___|| ____| |"<<endl;
	cout<<" \\ V / | | | | | |   | |  | | | \\___ \\|  _| | |"<<endl;
	cout<<"  | || |_| | |_| |   | |__| |_| |___) | |___|_| "<<endl;
	cout<<"  |_| \\___/ \\___/    |_____\\___/|____/|_____(_) "<<endl;
	cout<<"+ + + + + + + + + + + + + + + + + + + + + + + +"<<endl;
}

void Program::printColorText(std::string str, Battleship::Ascii::Program::Code colorCode)
{
    std::cout << "\033[" << colorCode << "m" << str << "\033[" << FG_DEFAULT << "m";
}

	Position Program::ParsePosition(string input)
    {
      char cColumn = toupper(input.at(0));
      char cRow = input.at(1);

	  int nColumn = (cColumn - 'A');
        Letters lColumn = (Letters)nColumn;

      string rowStr = input.substr(1);
      int nRow = std::stoi(rowStr);

	  Position outPosition;
	  outPosition.Column = lColumn;
	  outPosition.Row = nRow;
	  return outPosition;
    }

    Position Program::GetRandomPosition()
    {
      const int size = 8;
      srand((unsigned int) time(NULL));
      Letters lColumn = (Letters)(rand() % size);
      int nRow = (rand() % size);

      Position position(lColumn, nRow);
      return position;
    }

    void Program::InitializeGame()
    {
      InitializeMyFleet();

      InitializeEnemyFleet(enemyFleet);
    }

	void Program::InitializeMyFleet()
	{
		myFleet = GameController::GameController::InitializeShips();

		cout << "Please position your fleet (Game board has size from A to H and 1 to 8) :" << endl;
		for_each(myFleet.begin(), myFleet.end(), [](Ship &ship)
		{
			cout << endl;
		/*	cout << "Please enter the positions for the " << ship.Name << " (size: " << ship.Size << ")" << endl;
			for (int i = 1; i <= ship.Size; i++)
			{
        cout << "Enter position " << i << " of " << ship.Size << "\n";
				string input;
				getline(cin, input);
				Position inputPosition = ParsePosition(input);

				ship.AddPosition(inputPosition);
			}*/

      if (ship.Name == "Aircraft Carrier")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 1));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 2));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 3));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 4));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 5));
        totalHits += 5;
			}

		});
	}

	void Program::InitializeEnemyFleet(list<Ship>& Fleet)
	{
		Fleet = GameController::GameController::InitializeEnemyShips();
    totalHits = 0;
		for_each(Fleet.begin(), Fleet.end(), [](Ship& ship)
		{
			if (ship.Name == "Aircraft Carrier")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 1));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 2));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 3));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 4));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 5));
        totalHits += 5;
			}
			if (ship.Name == "Battleship")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 5));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 6));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 7));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::E, 8));
        totalHits += 4;
			}
			if (ship.Name == "Submarine")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::A, 3));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::B, 3));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 3));
        totalHits += 3;
			}
			if (ship.Name == "Destroyer")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::F, 8));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::G, 8));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::H, 8));
        totalHits += 3;
			}
			if (ship.Name == "Patrol Boat")
			{
				ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 5));
				ship.Positions.insert(ship.Positions.end(), Position(Letters::C, 6));
        totalHits += 2;
			}
		});
	}
  }
}
